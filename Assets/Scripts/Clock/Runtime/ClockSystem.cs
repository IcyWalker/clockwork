using System;
using UnityEngine;

namespace Clock.Runtime
{
    public class ClockSystem
    {
        private float minutes = 0;

        private float seconds = 0;

        private static ClockSystem instance;
        public float Seconds => seconds;

        public float Minutes => minutes;

        public static ClockSystem Instance => instance;

        public ClockSystem()
        {
            if (instance == null)
            {
                instance = this;

            }
            else
            {
                Debug.LogException(new Exception("Tried to launch a new clock system while one exists"));
            }
        }

        public void RequestedUpdate()
        {
            if (seconds > 59)
            {
                seconds -= 60;
                minutes = (minutes >= 23) ? 0 : minutes + 1;
            }
        }

        public void UpdateSeconds(float speedSeconds)
        {
            seconds += speedSeconds;
        }
    }
}