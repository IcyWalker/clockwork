using UnityEngine;

namespace Clock.Runtime
{
    public class ClockArmVisualiser : MonoBehaviour
    {
        [SerializeField] private float speedSeconds;
        [SerializeField] private RectTransform minHand;
        [SerializeField] private RectTransform secHand;
        [SerializeField] private float offset;
        private ClockSystem systemClock;
        private const int minutesInHour = 24;
        private const int secondsInMinute = 60;
        private const int degreesMax = 360;
        private void Start()
        {
            if (systemClock == null)
            {
                systemClock = new ClockSystem();
            }
        }

        private void Update()
        {
            systemClock.UpdateSeconds(speedSeconds * Time.deltaTime);
            systemClock.RequestedUpdate();

        
            var normalMin = (systemClock.Minutes / minutesInHour);
            var currentMin = (normalMin == 0) ? 0 : degreesMax * normalMin;
            minHand.localEulerAngles = new Vector3(0, 0, offset - currentMin);

            var normalSec = (systemClock.Seconds / secondsInMinute);
            var currentSec = (normalSec == 0) ? 0 : degreesMax * normalSec;
            secHand.localEulerAngles = new Vector3(0, 0, offset - currentSec);
        }
    }
}