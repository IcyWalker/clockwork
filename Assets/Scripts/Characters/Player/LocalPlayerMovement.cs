using System;
using System.Collections;
using Clock.Runtime;
using UnityEngine;
using Weather.Runtime;

namespace Characters.Player
{
    public class LocalPlayerMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rigidbody2D;
        [SerializeField] private Transform playerBody;
        [SerializeField] private float movementSpeed;
        private bool isDayCycle = true;
        private void Start()
        {
            if (rigidbody2D == null)
            {
                rigidbody2D = GetComponent<Rigidbody2D>();
            }

            StartCoroutine(WeatherSync());

        }

        private IEnumerator WeatherSync()
        {
            yield return new WaitForEndOfFrame();

            while (WeatherSwitch.Instance == null)
            {
                yield return new WaitForSeconds(0);
            }
            WeatherSwitch.Instance.DayStarted.AddListener(DayStarted);
            WeatherSwitch.Instance.NightStarted.AddListener(NightStarted);
        }

        private void DayStarted()
        {
            rigidbody2D.velocity = Vector2.zero;
            isDayCycle = true;
        }
        private void NightStarted()
        {
            rigidbody2D.velocity = Vector2.zero;
            isDayCycle = false;
        }
        private void Update()
        {
            if (ClockSystem.Instance == null)
            {
                return;
            }

            if (!isDayCycle)
            {
                return;
            }
            float playerVelocity = 0;
            if (Input.GetAxis("Horizontal") != 0)
            {
                var movementDirection = Input.GetAxis("Horizontal");
                bool directionRight = (movementDirection > 0);
                playerBody.localEulerAngles = new Vector3(0, directionRight?0:180, 0);
                playerVelocity =  movementDirection * (movementSpeed * Time.deltaTime);
                rigidbody2D.velocity = new Vector2(playerVelocity, rigidbody2D.velocity.y);
            }
        }
    }
}
