using System;
using Clock.Runtime;
using UnityEngine;
using UnityEngine.Events;

namespace Weather.Runtime
{
    public class WeatherSwitch : MonoBehaviour
    {
        public UnityEvent DayStarted;
        public UnityEvent NightStarted;
        [SerializeField] private int dayMin;
        [SerializeField] private int dayMax;
        private bool wasDay;
        private static WeatherSwitch instance;

        public static WeatherSwitch Instance => instance;

        private void Start()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Update()
        {
            if (ClockSystem.Instance == null)
            {
                return;
            }

            bool day = (Mathf.FloorToInt(ClockSystem.Instance.Seconds) >= dayMin &&
                        Mathf.FloorToInt(ClockSystem.Instance.Seconds) <= dayMax);

            if (wasDay != day)
            {
                wasDay = day;
                if (wasDay)
                {
                    DayStarted?.Invoke();
                }
                else
                {
                    NightStarted?.Invoke();
                }
            }
        }
    }
}