using System.Collections.Generic;
using UnityEngine;

public class WeatherEffect : MonoBehaviour
{
    [SerializeField] private List<Transform> layers;
    [SerializeField] private float movementSpeedOfLayer;
    [SerializeField] private int layerMaxCount;
    [SerializeField] private Transform killTransform;

    void Update()
    {
        while (layers.Count < layerMaxCount)
        {
            var BackgroundLayer = Instantiate(layers[0], this.transform, true);
            var layerDistance = layers[0].transform.localPosition.x - layers[1].transform.localPosition.x;
            Vector3 lastCurrentlHolder = layers[^1].transform.localPosition;
            lastCurrentlHolder.x += -layerDistance;
            BackgroundLayer.transform.localPosition = lastCurrentlHolder;
            layers.Add(BackgroundLayer);
        }

        Transform targetLayer = null;
        foreach (Transform layer in layers)
        {
            if (layer.position.x < killTransform.position.x)
            {
                targetLayer = layer;
                break;
            }
        }

        if (targetLayer == null)
        {
            return;
        }
    // future plan instead of removing just move to back and reuse resource
        layers.Remove(targetLayer);
        Destroy(targetLayer.gameObject);
    }

    private void FixedUpdate()
    {
        Vector3 layerLocalPosition = transform.localPosition;
        layerLocalPosition.x -= (movementSpeedOfLayer * Time.deltaTime);
        transform.localPosition = layerLocalPosition;
    }
}